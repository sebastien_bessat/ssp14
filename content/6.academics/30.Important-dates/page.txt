title: Important Dates
-
date: Nov 2013
-
content: 
<table class="table table-bordered table-striped">
	<tr>
		<td>Arrival Day and Registration</td>
		<td>Saturday, June 7</td>
	</tr>
	<tr>
		<td>Welcome Dinner and Participants Self Introductions</td>
		<td>Saturday, June 7</td>
	</tr>
	<tr>
		<td>Orientation</td>
		<td>Sunday, June 8</td>
	</tr>
	<tr>
		<td>Logistics Briefing and Staff Introduction</td>
		<td>Monday, June 9</td>
	</tr>
	<tr>
		<td>Class Picture, Opening Ceremony and Reception</td>
		<td>Monday, June 24</td>
	</tr>
	<tr>
		<td>TP Selection Due</td>
		<td>Tuesday, June 10. at 14:00</td>
	</tr>
	<tr>
		<td>Core Lecture Quiz 1</td>
		<td>Saturday, June 13, at 12:30</td>
	</tr>
	<tr>
		<td>Department Selection Due</td>
		<td>Wednesday, June 18. at 14:00</td>
	</tr>
	<tr>
		<td>Core Lecture Quiz 2</td>
		<td>Saturday, June 20, at 12:30</td>
	</tr>
	<tr>
		<td>Workshop Seletction Due</td>
		<td>Wednesday, June 20. at 14:00</td>
	</tr>
	<tr>
		<td>Core Lecture Quiz 3</td>
		<td>Friday, June 27, at 12:30</td>
	</tr>
	<tr>
		<td>Robotics Competition</td>
		<td>Friday, June 27</td>
	</tr>
	<tr>
		<td>Core Lecture Quiz 4</td>
		<td>Thursday, July 2, at 12:30</td>
	</tr>
	<tr>
		<td>Core Lecture Essay Exam</td>
		<td>Thursday, July 4</td>
	</tr>
	<tr>
		<td>TP TPP</td>
		<td>Tuesday, July 15, at 14:00</td>
	</tr>
	<tr>
		<td>Rocket Launch</td>
		<td>Saturday, July 19</td>
	</tr>
	<tr>
		<td>TP Internal Review</td>
		<td>Friday, July 25</td>
	</tr>
	<tr>
		<td>TP Executive Summary Cover Art</td>
		<td>Monday, July 28. at 08:30</td>
	</tr>
	<tr>
		<td>TP Executive Summary and Report Draft</td>
		<td>Friday, August 1, at 18:30</td>
	</tr>
	<tr>
		<td>TP Executive Summary Due</td>
		<td>Monday, August 4, at 08:30</td>
	</tr>
	<tr>
		<td>TP Final Report Due</td>
		<td>Tuesday, August 5, at 18:30</td>
	</tr>
	<tr>
		<td>TP Final Presentation</td>
		<td>Thursday, August 7</td>
	</tr>
	<tr>
		<td>Deregistration</td>
		<td>Friday, August 8</td>
	</tr>
	<tr>
		<td>Closing Ceremony & Reception</td>
		<td>Friday, August 8</td>
	</tr>
	<tr>
		<td>Departure Day</td>
		<td>Saturday, August 9</td>
	</tr>
</table>

