title: About us
-
content: 

### SSP

The Space Studies Program (SSP) is an intense nine-week course for postgraduate students and professionals of all disciplines. The curriculum covers the principal space related fields, both non-technical and technical and ranges from policy and law, business and management and humanities to life sciences, engineering, physical sciences and space applications. The shared experience of an international, interactive working environment is an ideal networking forum leading to the creation of an extensive, international, multidisciplinary professional network.


Each year the SSP convenes in a different location around the world. Moving to a new city and country adds an exciting dynamic as well as new resources and expertise to the program. 
The SSP curriculum includes:

- Core Lectures covering fundamental concepts across all relevant disciplines,
- Theme days presenting keys/issues of space with an interdisciplinary approach,
- Hands-on workshops providing practical applications of the concepts presented in the lectures,
- Departmental Activities of the seven SSP departments providing in-depth lectures & workshops, professional visits, and individual research projects,
- Team Projects in which the SSP participants address a relevant space topic as an international, interdisciplinary, and intercultural team.

[More Information on ISU Website »](http://www.isunet.edu/ssp-curriculum)

### ISU

The International Space University is a private non-profit institution, formally recognized as an institute of higher education in France by the French Ministry of Education (decree MENS0400386A of 27 February 2004). It specializes in providing graduate-level training to the future leaders of the emerging global space community at its Central Campus in Strasbourg, France, and at locations around the world. In its two-month Space Studies Program and one-year Masters program, ISU offers its students a unique Core Curriculum covering all disciplines related to space programs and enterprises, space science, space engineering, systems engineering, space policy and law, business and management, and space and society. Both programs also involve an intense student research Team Project providing international graduate students and young space professionals the opportunity to solve complex problems by working together in an intercultural environment. 

Since its founding in 1987, ISU has graduated more than 3700 students from over 100 countries. Together with hundreds of ISU faculty and lecturers from around the world, ISU alumni comprise an extremely effective network of space professionals and leaders that actively facilitates individual career growth, professional activities and international space cooperation. 

Visit our website: <http://www.isunet.edu>

### Contact us

<span>
	Program General & Academics:
	<script>
		emailE = ('isu.academics' + '@' + 'isunet.edu')
		document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
	</script>
</span>

<span>
	Logistics Enqueries : 
	<script>
		emailE = ('isu.logistics' + '@' + 'isunet.edu')
		document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
	</script>
</span>